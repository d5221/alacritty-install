#!/bin/bash

#Clone Alacritty repo

git clone https://github.com/alacritty/alacritty.git &&
cd alacritty &&

#To make sure you have the right Rust compiler installed

rustup override set stable &&
rustup update stable &&

#Building

cargo build --release

#Post-Building

infocmp alacritty &&
sudo tic -xe alacritty,alacritty-direct extra/alacritty.info &&

#Desktop Entry

sudo cp target/release/alacritty /usr/local/bin &&
sudo cp extra/logo/alacritty-term.svg /usr/share/pixmaps/Alacritty.svg &&
sudo desktop-file-install extra/linux/Alacritty.desktop &&
sudo update-desktop-database &&

#Man Page

sudo mkdir -p /usr/local/share/man/man1 &&
gzip -c extra/alacritty.man | sudo tee /usr/local/share/man/man1/alacritty.1.gz > /dev/null &&
gzip -c extra/alacritty-msg.man | sudo tee /usr/local/share/man/man1/alacritty-msg.1.gz > /dev/null
